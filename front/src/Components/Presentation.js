import React, { Component } from "react";

class Presentation extends Component {
  render() {
    return (
        <section id="Presentation" className="justify-content-center text-center wrapper">
            <div className="header-presentation">
            </div>
            <svg className="img-fluid" viewBox="0 0 1440 320"><path fill="#0099ff" fillOpacity="1" d="M0,256L12.6,245.3C25.3,235,51,213,76,208C101.1,203,126,213,152,197.3C176.8,181,202,139,227,106.7C252.6,75,278,53,303,64C328.4,75,354,117,379,138.7C404.2,160,429,160,455,144C480,128,505,96,531,69.3C555.8,43,581,21,606,26.7C631.6,32,657,64,682,106.7C707.4,149,733,203,758,240C783.2,277,808,299,834,293.3C858.9,288,884,256,909,218.7C934.7,181,960,139,985,138.7C1010.5,139,1036,181,1061,186.7C1086.3,192,1112,160,1137,138.7C1162.1,117,1187,107,1213,122.7C1237.9,139,1263,181,1288,213.3C1313.7,245,1339,267,1364,261.3C1389.5,256,1415,224,1427,208L1440,192L1440,0L1427.4,0C1414.7,0,1389,0,1364,0C1338.9,0,1314,0,1288,0C1263.2,0,1238,0,1213,0C1187.4,0,1162,0,1137,0C1111.6,0,1086,0,1061,0C1035.8,0,1011,0,985,0C960,0,935,0,909,0C884.2,0,859,0,834,0C808.4,0,783,0,758,0C732.6,0,707,0,682,0C656.8,0,632,0,606,0C581.1,0,556,0,531,0C505.3,0,480,0,455,0C429.5,0,404,0,379,0C353.7,0,328,0,303,0C277.9,0,253,0,227,0C202.1,0,177,0,152,0C126.3,0,101,0,76,0C50.5,0,25,0,13,0L0,0Z"></path></svg>
            <div className="second-face">
                    <img src="career_progress.svg" className="pip img-fluid img-pres" alt="Responsive image"></img>
                    <img src="forming_ideas.svg" id="img-idea" className="pip img-fluid img-pres" alt="Responsive image"></img>
                </div>
            <h1>Is not a role, but a vision ...</h1>
            <small className="text-muted blockquote-footer ">The word “DevOps” is a mashup of “development’ and “operations” but it represents a set of ideas and practices much larger than those two terms alone, or together.</small>
            <div className="content bounce bottom">
                <br />
                <br />
                <a href="#Concept">
                    <div className="arrow"></div>
                </a>  
            </div>
        </section>
    )
   }
}
export default Presentation;