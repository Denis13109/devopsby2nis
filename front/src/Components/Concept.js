import React, { Component } from "react";
import Dev from './Dev.js';
import Ops from './Ops.js';

class Concept extends Component {
  render() {
    return (
        <section id="Concept" className="justify-content-center text-center">
            <svg className="bordersvg" viewBox="0 0 1440 320"><path fill="#0099ff" fillOpacity="1" d="M0,256L16,256C32,256,64,256,96,240C128,224,160,192,192,149.3C224,107,256,53,288,69.3C320,85,352,171,384,181.3C416,192,448,128,480,133.3C512,139,544,213,576,202.7C608,192,640,96,672,85.3C704,75,736,149,768,197.3C800,245,832,267,864,277.3C896,288,928,288,960,256C992,224,1024,160,1056,154.7C1088,149,1120,203,1152,197.3C1184,192,1216,128,1248,133.3C1280,139,1312,213,1344,213.3C1376,213,1408,139,1424,101.3L1440,64L1440,320L1424,320C1408,320,1376,320,1344,320C1312,320,1280,320,1248,320C1216,320,1184,320,1152,320C1120,320,1088,320,1056,320C1024,320,992,320,960,320C928,320,896,320,864,320C832,320,800,320,768,320C736,320,704,320,672,320C640,320,608,320,576,320C544,320,512,320,480,320C448,320,416,320,384,320C352,320,320,320,288,320C256,320,224,320,192,320C160,320,128,320,96,320C64,320,32,320,16,320L0,320Z"></path></svg>
            <div className="content">
                <h1>Concept</h1>
                <h5>
                DevOps is an approach to culture, automation, and platform design intended to deliver increased business value and responsiveness through rapid, high-quality service delivery. This is all made possible through fast-paced, iterative IT service delivery. DevOps means linking legacy apps with newer cloud-native apps and infrastructure.
                </h5>
                <img src="Infinity_Loop.png" id="img-concept" className="img-fluid" alt="Responsive image"></img>
                <h5>
                DevOps describes approaches to speeding up the processes by which an idea (like a new software feature, a request for enhancement, or a bug fix) goes from development to deployment in a production environment where it can provide value to the user. 
                These approaches require that development teams and operations teams communicate frequently and approach their work with empathy for their teammates. Scalability and flexible provisioning are also necessary. With DevOps, those that need power the most, get it—through self service and automation. Developers, usually coding in a standard development environment, work closely with IT operations to speed software builds, tests, and releases—without sacrificing reliability.
                </h5>
                <br />
                <br />
                <div className="second-face">
                    <Dev />
                    <Ops />
                </div>
            </div>
            <svg className="bordersvg" viewBox="0 0 1440 320"><path fill="#0099ff" fillOpacity="1" d="M0,256L12.6,245.3C25.3,235,51,213,76,208C101.1,203,126,213,152,197.3C176.8,181,202,139,227,106.7C252.6,75,278,53,303,64C328.4,75,354,117,379,138.7C404.2,160,429,160,455,144C480,128,505,96,531,69.3C555.8,43,581,21,606,26.7C631.6,32,657,64,682,106.7C707.4,149,733,203,758,240C783.2,277,808,299,834,293.3C858.9,288,884,256,909,218.7C934.7,181,960,139,985,138.7C1010.5,139,1036,181,1061,186.7C1086.3,192,1112,160,1137,138.7C1162.1,117,1187,107,1213,122.7C1237.9,139,1263,181,1288,213.3C1313.7,245,1339,267,1364,261.3C1389.5,256,1415,224,1427,208L1440,192L1440,0L1427.4,0C1414.7,0,1389,0,1364,0C1338.9,0,1314,0,1288,0C1263.2,0,1238,0,1213,0C1187.4,0,1162,0,1137,0C1111.6,0,1086,0,1061,0C1035.8,0,1011,0,985,0C960,0,935,0,909,0C884.2,0,859,0,834,0C808.4,0,783,0,758,0C732.6,0,707,0,682,0C656.8,0,632,0,606,0C581.1,0,556,0,531,0C505.3,0,480,0,455,0C429.5,0,404,0,379,0C353.7,0,328,0,303,0C277.9,0,253,0,227,0C202.1,0,177,0,152,0C126.3,0,101,0,76,0C50.5,0,25,0,13,0L0,0Z"></path></svg>
        </section>
    )
   }
}
export default Concept;