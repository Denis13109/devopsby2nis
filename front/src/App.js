import React from 'react';
import './App.css';
import Navbar from './Components/Navbar.js';
import Concept from './Components/Concept.js';
import Agility from './Components/Agility.js';
import CiCd from './Components/CiCd.js';
import Presentation from './Components/Presentation.js';
import Tools from './Components/Tools.js';

function App() {
  return (
    <div className="App">
      <Navbar />
      <Presentation />
      <Concept />
      <Agility />
      <CiCd />
      <Tools />
    </div>
  );
}

export default App;
